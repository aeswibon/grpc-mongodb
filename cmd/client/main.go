package main

import (
	"log"

	"gitlab.com/aeswibon/grpc-mongodb/client"
	pb "gitlab.com/aeswibon/grpc-mongodb/pb/proto/messages"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

const (
	address = "0.0.0.0:50051"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())

	if err != nil {
		log.Fatalf("failed to connect: %v", err)
	}

	defer conn.Close()

	// Sign Up
	if false {
		authClient := client.NewAuthClient(conn)
		newUser := &pb.RegisterRequest{
			Name:            "Jane Smith",
			Email:           "janesmith@gmail.com",
			Password:        "password123",
			PasswordConfirm: "password123",
		}
		authClient.Register(newUser)
	}

	// Sign In
	if false {
		authClient := client.NewAuthClient(conn)
		credentials := &pb.LoginRequest{
			Email:    "janesmith@gmail.com",
			Password: "password123",
		}
		authClient.Login(credentials)
	}

	// Get Me
	if false {
		userClient := client.NewUserClient(conn)
		id := &pb.ProfileRequest{
			Id: "628cffb91e50302d360c1a2c",
		}
		userClient.Profile(id)

	}

	// List Posts
	if false {
		postClient := client.NewPostClient(conn)

		var page int64 = 1
		var limit int64 = 10
		args := &pb.GetPostsRequest{
			Page:  &page,
			Limit: &limit,
		}
		postClient.ListPosts(args)
	}

	// Create Post
	if false {
		postClient := client.NewPostClient(conn)

		args := &pb.CreatePostRequest{
			Title:   "My second gRPC post with joy",
			Content: "It's always good to learn new technologies",
			User:    "62908e0a42a608d5aeae2f64",
			Image:   "default.png",
		}
		postClient.CreatePost(args)
	}

	// Update Post
	if false {
		postClient := client.NewPostClient(conn)

		title := "My new updated title for my blog"
		args := &pb.UpdatePostRequest{
			Id:    "629169e00a6c7cfd24e2129d",
			Title: &title,
		}
		postClient.UpdatePost(args)
	}

	// Get Post
	if false {
		postClient := client.NewPostClient(conn)

		args := &pb.PostRequest{
			Id: "629169e00a6c7cfd24e2129d",
		}
		postClient.GetPost(args)
	}

	// Delete Post
	if false {
		postClient := client.NewPostClient(conn)

		args := &pb.PostRequest{
			Id: "629147ff3c92aed11d49394b",
		}
		postClient.DeletePost(args)
	}
}
