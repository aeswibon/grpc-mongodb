package main

import (
	"context"
	"log"
	"net"

	"gitlab.com/aeswibon/grpc-mongodb/config"
	"gitlab.com/aeswibon/grpc-mongodb/gapi/auth"
	"gitlab.com/aeswibon/grpc-mongodb/gapi/post"
	"gitlab.com/aeswibon/grpc-mongodb/gapi/user"
	pb "gitlab.com/aeswibon/grpc-mongodb/pb/proto/services"
	"gitlab.com/aeswibon/grpc-mongodb/services"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	app := config.App()
	env := app.Env
	db := app.Mongo.Database(env.DBName)
	defer app.CloseDBConnection()

	// // Connect to Redis
	// redisClient = redis.NewClient(&redis.Options{
	// 	Addr: env.RedisURI,
	// })

	// if _, err := redisClient.Ping(ctx).Result(); err != nil {
	// 	panic(err)
	// }

	// err := redisClient.Set(ctx, "test", "Welcome to Golang with Redis and MongoDB", 0).Err()
	// if err != nil {
	// 	panic(err)
	// }

	// fmt.Println("Redis client connected successfully...")

	ctx := context.TODO()

	// Collections
	authCollection := db.Collection("users")
	postCollection := db.Collection("posts")

	log.Println("Creating servicess...")
	// 👇 Instantiate the services
	userService := services.NewUserServiceImpl(ctx, authCollection)
	authService := services.NewAuthService(ctx, authCollection)
	postService := services.NewPostService(ctx, postCollection)

	log.Println("Creating gRPC servers...")
	authServer, err := auth.NewGrpcAuthServer(*env, authService, userService, authCollection)
	if err != nil {
		log.Fatal("cannot create grpc authServer: ", err)
	}

	userServer, err := user.NewGrpcUserServer(*env, userService, authCollection)
	if err != nil {
		log.Fatal("cannot create grpc userServer: ", err)
	}

	postServer, err := post.NewGrpcPostServer(postCollection, postService)
	if err != nil {
		log.Fatal("cannot create grpc postServer: ", err)
	}

	grpcServer := grpc.NewServer()
	reflection.Register(grpcServer)

	log.Println("Registering gRPC servers...")
	pb.RegisterAuthServiceServer(grpcServer, authServer)
	pb.RegisterUserServiceServer(grpcServer, userServer)
	pb.RegisterPostServiceServer(grpcServer, postServer)

	log.Println("Starting gRPC servers...")
	listener, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatal("cannot create grpc server: ", err)
	}

	log.Printf("start gRPC server on %s", listener.Addr().String())
	err = grpcServer.Serve(listener)
	if err != nil {
		log.Fatal("cannot create grpc server: ", err)
	}
}
