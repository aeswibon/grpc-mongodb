package config

import "go.mongodb.org/mongo-driver/mongo"

// Application struct
type Application struct {
	Env   *Config
	Mongo *mongo.Client
}

// App method to create new application
func App() Application {
	app := &Application{}
	app.Env = LoadConfig()
	app.Mongo = NewMongoDatabase(app.Env)
	return *app
}

// CloseDBConnection method to remove mongo connection
func (app *Application) CloseDBConnection() {
	CloseMongoDBConnection(app.Mongo)
}
