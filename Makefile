.PHONY: dev dev-down go proto

dev:
	@echo "Starting development environment..."
	@docker-compose up -d

dev-down:
	@echo "Stopping development environment..."
	@docker-compose down

upgrade:
	@echo "Upgrading dependencies..."
	@go get -u ./...

go:
	@echo "Building go binary..."
	@air

generate: generate-messages generate-services

generate-messages:
	@echo "Compiling proto code for messages..."
	@protoc --proto_path=./ --go_out=pb --go_opt=paths=source_relative --go-grpc_out=pb --go-grpc_opt=paths=source_relative proto/messages/*.proto

generate-services:
	@echo "Compiling proto code for services..."
	@protoc --proto_path=./ --go_out=pb --go_opt=paths=source_relative --go-grpc_out=pb --go-grpc_opt=paths=source_relative proto/services/*.proto