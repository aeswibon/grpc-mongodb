package client

import (
	"context"
	"fmt"
	"log"
	"time"

	"gitlab.com/aeswibon/grpc-mongodb/pb/proto/messages"
	"gitlab.com/aeswibon/grpc-mongodb/pb/proto/services"
	"google.golang.org/grpc"
)

// UserClient struct
type UserClient struct {
	service services.UserServiceClient
}

// NewUserClient function to create new UserClient
func NewUserClient(conn *grpc.ClientConn) *UserClient {
	service := services.NewUserServiceClient(conn)
	return &UserClient{service}
}

// Profile function to get user profile
func (uc *UserClient) Profile(credentials *messages.ProfileRequest) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Millisecond*5000))
	defer cancel()

	res, err := uc.service.Profile(ctx, credentials)
	if err != nil {
		log.Fatalf("GeMe: %v", err)
	}
	fmt.Println(res)
}
