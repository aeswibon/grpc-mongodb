package client

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"

	"gitlab.com/aeswibon/grpc-mongodb/pb/proto/messages"
	"gitlab.com/aeswibon/grpc-mongodb/pb/proto/services"
	"google.golang.org/grpc"
)

// PostClient struct
type PostClient struct {
	service services.PostServiceClient
}

// NewPostClient function to create new PostClient
func NewPostClient(conn *grpc.ClientConn) *PostClient {
	service := services.NewPostServiceClient(conn)
	return &PostClient{service}
}

// CreatePost function to create new post
func (pc *PostClient) CreatePost(args *messages.CreatePostRequest) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Millisecond*5000))
	defer cancel()

	res, err := pc.service.CreatePost(ctx, args)
	if err != nil {
		log.Fatalf("CreatePost: %v", err)
	}
	fmt.Println(res)
}

// ListPosts function to list posts
func (pc *PostClient) ListPosts(args *messages.GetPostsRequest) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Millisecond*5000))
	defer cancel()

	stream, err := pc.service.GetPosts(ctx, args)
	if err != nil {
		log.Fatalf("ListPosts: %v", err)
	}

	for {
		res, err := stream.Recv()

		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatalf("ListPosts: %v", err)
		}

		fmt.Println(res)
	}
}

// GetPost function to get post
func (pc *PostClient) GetPost(args *messages.PostRequest) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Millisecond*5000))
	defer cancel()

	res, err := pc.service.GetPost(ctx, args)
	if err != nil {
		log.Fatalf("GetPost: %v", err)
	}
	fmt.Println(res)
}

// UpdatePost function to update post
func (pc *PostClient) UpdatePost(args *messages.UpdatePostRequest) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Millisecond*5000))
	defer cancel()

	res, err := pc.service.UpdatePost(ctx, args)
	if err != nil {
		log.Fatalf("UpdatePost: %v", err)
	}
	fmt.Println(res)
}

// DeletePost function to delete post
func (pc *PostClient) DeletePost(args *messages.PostRequest) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Millisecond*5000))
	defer cancel()

	_, err := pc.service.DeletePost(ctx, args)
	if err != nil {
		log.Fatalf("DeletePost: %v", err)
	}
	fmt.Println("Post deleted successfully")
}
