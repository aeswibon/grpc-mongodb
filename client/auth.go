package client

import (
	"context"
	"fmt"
	"log"
	"time"

	"gitlab.com/aeswibon/grpc-mongodb/pb/proto/messages"
	"gitlab.com/aeswibon/grpc-mongodb/pb/proto/services"
	"google.golang.org/grpc"
)

// AuthClient struct
type AuthClient struct {
	service services.AuthServiceClient
}

// NewAuthClient function to create new AuthClient
func NewAuthClient(conn *grpc.ClientConn) *AuthClient {
	service := services.NewAuthServiceClient(conn)
	return &AuthClient{service}
}

// Login function to login user
func (au *AuthClient) Login(credentials *messages.LoginRequest) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	res, err := au.service.Login(ctx, credentials)
	if err != nil {
		log.Fatalf("SignInUser: %v", err)
	}
	fmt.Println(res)
}

// Register function to sign up user
func (au *AuthClient) Register(credentials *messages.RegisterRequest) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Millisecond*5000))
	defer cancel()

	res, err := au.service.Register(ctx, credentials)
	if err != nil {
		log.Fatalf("SignUpUser: %v", err)
	}
	fmt.Println(res)
}
