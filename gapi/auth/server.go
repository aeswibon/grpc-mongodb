package auth

import (
	"gitlab.com/aeswibon/grpc-mongodb/config"
	pb "gitlab.com/aeswibon/grpc-mongodb/pb/proto/services"
	"gitlab.com/aeswibon/grpc-mongodb/services"
	"go.mongodb.org/mongo-driver/mongo"
)

// AuthServer struct
type AuthServer struct {
	pb.UnimplementedAuthServiceServer
	config         config.Config
	authService    services.AuthService
	userService    services.UserService
	userCollection *mongo.Collection
}

// NewGrpcAuthServer creates a new auth server
func NewGrpcAuthServer(config config.Config, authService services.AuthService,
	userService services.UserService, userCollection *mongo.Collection) (*AuthServer, error) {

	authServer := &AuthServer{
		config:         config,
		authService:    authService,
		userService:    userService,
		userCollection: userCollection,
	}

	return authServer, nil
}
