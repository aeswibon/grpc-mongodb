package auth

import (
	"context"

	pb "gitlab.com/aeswibon/grpc-mongodb/pb/proto/messages"
	"gitlab.com/aeswibon/grpc-mongodb/utils"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Login function to login user
func (authServer *AuthServer) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	user, err := authServer.userService.FindUserByEmail(req.GetEmail())
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, status.Errorf(codes.InvalidArgument, "Invalid email or password")
		}
		return nil, status.Errorf(codes.Internal, err.Error())
	}
	if !user.Verified {
		return nil, status.Errorf(codes.PermissionDenied, "You are not verified, please verify your email to login")
	}

	if err := utils.VerifyPassword(user.Password, req.GetPassword()); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid email or Password")
	}

	// Generate Access token
	accessToken, err := utils.CreateToken(authServer.config.AccessTokenExpiresIn, user.ID, authServer.config.AccessTokenPrivateKey)
	if err != nil {
		return nil, status.Errorf(codes.PermissionDenied, err.Error())
	}

	// Generate Refresh token
	refreshToken, err := utils.CreateToken(authServer.config.RefreshTokenExpiresIn, user.ID, authServer.config.RefreshTokenPrivateKey)
	if err != nil {
		return nil, status.Errorf(codes.PermissionDenied, err.Error())
	}

	res := &pb.LoginResponse{
		Status:       "success",
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}

	return res, nil
}
