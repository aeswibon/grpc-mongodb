package post

import (
	pb "gitlab.com/aeswibon/grpc-mongodb/pb/proto/services"
	"gitlab.com/aeswibon/grpc-mongodb/services"
	"go.mongodb.org/mongo-driver/mongo"
)

// PostServer struct
type PostServer struct {
	pb.UnimplementedPostServiceServer
	postCollection *mongo.Collection
	postService    services.PostService
}

// NewGrpcPostServer creates a new post server
func NewGrpcPostServer(postCollection *mongo.Collection, postService services.PostService) (*PostServer, error) {
	postServer := &PostServer{
		postCollection: postCollection,
		postService:    postService,
	}

	return postServer, nil
}
