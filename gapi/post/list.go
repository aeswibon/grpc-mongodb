package post

import (
	messages "gitlab.com/aeswibon/grpc-mongodb/pb/proto/messages"
	services "gitlab.com/aeswibon/grpc-mongodb/pb/proto/services"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// GetPosts function to list post with pagination
func (postServer *PostServer) GetPosts(req *messages.GetPostsRequest, stream services.PostService_GetPostsServer) error {
	var page = req.GetPage()
	var limit = req.GetLimit()

	posts, err := postServer.postService.FindPosts(int(page), int(limit))
	if err != nil {
		return status.Errorf(codes.Internal, err.Error())
	}

	for _, post := range posts {
		stream.Send(&messages.Post{
			Id:        post.ID.Hex(),
			Title:     post.Title,
			Content:   post.Content,
			Image:     post.Image,
			CreatedAt: timestamppb.New(post.CreateAt),
			UpdatedAt: timestamppb.New(post.UpdatedAt),
		})
	}

	return nil
}
