package post

import (
	"context"
	"strings"

	pb "gitlab.com/aeswibon/grpc-mongodb/pb/proto/messages"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// DeletePost function to delete post
func (postServer *PostServer) DeletePost(ctx context.Context, req *pb.PostRequest) (*pb.DeletePostResponse, error) {
	postID := req.GetId()

	if err := postServer.postService.DeletePost(postID); err != nil {
		if strings.Contains(err.Error(), "Id exists") {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	res := &pb.DeletePostResponse{
		Success: true,
	}

	return res, nil
}
