package user

import (
	"gitlab.com/aeswibon/grpc-mongodb/config"
	pb "gitlab.com/aeswibon/grpc-mongodb/pb/proto/services"
	"gitlab.com/aeswibon/grpc-mongodb/services"
	"go.mongodb.org/mongo-driver/mongo"
)

// UserServer struct
type UserServer struct {
	pb.UnimplementedUserServiceServer
	config         config.Config
	userService    services.UserService
	userCollection *mongo.Collection
}

// NewGrpcUserServer creates a new user server
func NewGrpcUserServer(config config.Config, userService services.UserService, userCollection *mongo.Collection) (*UserServer, error) {
	userServer := &UserServer{
		config:         config,
		userService:    userService,
		userCollection: userCollection,
	}

	return userServer, nil
}
