package user

import (
	"context"

	pb "gitlab.com/aeswibon/grpc-mongodb/pb/proto/messages"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// Profile function to get user by id
func (userServer *UserServer) Profile(ctx context.Context, req *pb.ProfileRequest) (*pb.UserResponse, error) {
	id := req.GetId()
	user, err := userServer.userService.FindUserByID(id)

	if err != nil {
		return nil, status.Errorf(codes.Unimplemented, err.Error())
	}

	res := &pb.UserResponse{
		User: &pb.User{
			Id:        user.ID.Hex(),
			Name:      user.Name,
			Email:     user.Email,
			Role:      user.Role,
			CreatedAt: timestamppb.New(user.CreatedAt),
			UpdatedAt: timestamppb.New(user.UpdatedAt),
		},
	}
	return res, nil
}
