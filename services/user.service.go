package services

import "gitlab.com/aeswibon/grpc-mongodb/models"

// UserService interface
type UserService interface {
	FindUserByID(id string) (*models.DBResponse, error)
	FindUserByEmail(email string) (*models.DBResponse, error)
	UpdateUserByID(id string, data *models.UpdateInput) (*models.DBResponse, error)
}
