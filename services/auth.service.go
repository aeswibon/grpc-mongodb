package services

import "gitlab.com/aeswibon/grpc-mongodb/models"

// AuthService interface
type AuthService interface {
	Register(*models.SignUpInput) (*models.DBResponse, error)
	Login(*models.SignInInput) (*models.DBResponse, error)
}
