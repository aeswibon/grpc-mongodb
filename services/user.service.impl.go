package services

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/aeswibon/grpc-mongodb/models"
	"gitlab.com/aeswibon/grpc-mongodb/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// UserServiceImpl struct
type UserServiceImpl struct {
	collection *mongo.Collection
	ctx        context.Context
}

// NewUserServiceImpl function to create new UserServiceImpl
func NewUserServiceImpl(ctx context.Context, collection *mongo.Collection) UserService {
	return &UserServiceImpl{collection, ctx}
}

// FindUserByID function to find user by id
func (us *UserServiceImpl) FindUserByID(id string) (*models.DBResponse, error) {
	oid, _ := primitive.ObjectIDFromHex(id)

	var user *models.DBResponse

	query := bson.M{"_id": oid}
	err := us.collection.FindOne(us.ctx, query).Decode(&user)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return &models.DBResponse{}, err
		}
		return nil, err
	}

	return user, nil
}

// FindUserByEmail function to find user by email
func (us *UserServiceImpl) FindUserByEmail(email string) (*models.DBResponse, error) {
	var user *models.DBResponse

	query := bson.M{"email": strings.ToLower(email)}
	err := us.collection.FindOne(us.ctx, query).Decode(&user)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return &models.DBResponse{}, err
		}
		return nil, err
	}

	return user, nil
}

// UpdateUserByID function to update user by id
func (us *UserServiceImpl) UpdateUserByID(id string, data *models.UpdateInput) (*models.DBResponse, error) {
	doc, err := utils.ToDoc(data)
	if err != nil {
		return &models.DBResponse{}, err
	}

	fmt.Println(data)

	obID, _ := primitive.ObjectIDFromHex(id)

	query := bson.D{{Key: "_id", Value: obID}}
	update := bson.D{{Key: "$set", Value: doc}}
	result := us.collection.FindOneAndUpdate(us.ctx, query, update, options.FindOneAndUpdate().SetReturnDocument(1))

	var updatedUser *models.DBResponse
	if err := result.Decode(&updatedUser); err != nil {
		return nil, errors.New("no document with that id exists")
	}

	return updatedUser, nil
}
